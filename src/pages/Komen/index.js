import React, { useEffect, useState } from "react";
import styles from "./forum.module.css";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";

function Forum() {
  const [data, setData] = useState([]);
  const [comment, setComment] = useState("");
  const navigate = useNavigate();
  const params = useParams();
  useEffect(() => {
    axios
      .get("http://localhost:8000/forum/" + params.id)
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        alert(error);
      });
  }, []);
  const handleCommentChange = (event) => {
    setComment(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (comment.length > 0) {
      const jwtToken = localStorage.getItem("jwtToken");
      const headers = {
        Authorization: `Bearer ${jwtToken}`,
      };
      await axios
        .get("http://localhost:8000/auth/", { headers })
        .then(async (response) => {
          if (response.status === 200) {
            try {
              const response = await axios.post(
                "http://localhost:8000/forum/komen",
                {
                  konten: comment,
                  forumId: params.id,
                },
                {
                  headers: headers,
                }
              );
              window.location.reload();
            } catch (error) {
              // Handle error
              console.error(error);
            }
          } else if (response.status === 403) {
            return navigate("/forum/login");
          }
        })
        .catch((error) => {
          return navigate("/login");
        });
    }
  };

  return (
    <body className={styles.body}>
      <main>
        <header className={styles.header}>
          <nav className="navbar navbar-expand-lg navbar-light pt-5">
            <div class="container">
              <a class="navbar-brand" href="#">
                Logo
              </a>
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span class="navbar-toggler-icon"></span>
              </button>
              <div
                class="collapse navbar-collapse justify-content-end"
                id="navbarNav"
              >
                <ul class="navbar-nav my-auto">
                  <li class="nav-item active px-3">
                    <a class="nav-link" href="#">
                      Beranda <span class="sr-only"></span>
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Kategori
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Event
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Tentang
                    </a>
                  </li>
                </ul>
                <div class="navbar-nav px-5">
                  <button className={styles.btnLight} onClick="">
                    Masuk
                  </button>
                </div>
              </div>
            </div>
          </nav>
          <div className="container pb-4">
            <h1 className="text-center font-weight-bold pt-3">
              Ngevent Support Community
            </h1>
            <h4 className="text-center">
              Bertanya. Temukan Jawaban. Memecahkan masalah bersama Ngevent.
            </h4>
          </div>
        </header>
        {/* Main Content */}
        <div className="container">
          <div
            className="text-right"
            style={{ marginTop: "260px", marginRight: "49px" }}
          ></div>
          <div className="mt-4">
            <div className="row">
              <div className="col-2">
                <div className="position-fixed">
                  <input
                    className={styles.Search}
                    type="text"
                    placeholder="Cari Topik"
                  />
                  <ul class="list-group mt-4">
                    <li class="my-4 py-2  px-3 d-flex justify-content-between align-items-center selected">
                      All
                      <span class="badge badge-light badge-pill">14</span>
                    </li>
                    <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # Pembayaran
                      <span class="badge badge-light badge-pill">2</span>
                    </li>
                    <li class=" my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # refund
                      <span class="badge badge-light badge-pill">1</span>
                    </li>
                    <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # ide
                      <span class="badge badge-light badge-pill">1</span>
                    </li>
                    <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # pelatihan
                      <span class="badge badge-light badge-pill">1</span>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="col-9" style={{ marginLeft: "50px" }}>
                <div class="card border-top-0 border-right-0 border-bottom-0 border-light mb-5">
                  <div class="card-body">
                    <h4 class="pt-1 font-weight-bold">
                      {data.forumInduk && data.forumInduk.judul}
                    </h4>
                    <div className="mt-3 d-flex justify-content-start">
                      <img
                        src="https://media.licdn.com/dms/image/D5635AQFnm3Yjq49XMw/profile-framedphoto-shrink_200_200/0/1668648681946?e=1687338000&v=beta&t=p5zZpH4UJIQUIR5xBXgIIEyfl3lqlmrHdf_WHqFxG4U"
                        class="rounded-circle d-block"
                        alt="profil"
                        height="50"
                        width="50"
                      />
                      <div className="ml-2">
                        <h6 class="card-title">Ariawan</h6>
                        <p
                          class="card-subtitle mb-2 text-muted"
                          style={{ fontSize: "13px" }}
                        >
                          {data.forumInduk && data.forumInduk.createdAt}
                        </p>
                      </div>
                    </div>
                    <p class="mt-2">
                      {data.forumInduk && data.forumInduk.konten}
                    </p>

                    <div className="mt-3">
                      <label for="exampleFormControlSelect1">
                        Tambahkan Kometar
                      </label>
                      <textarea
                        class="form-control"
                        id="exampleFormControlTextarea1"
                        rows="3"
                        onChange={handleCommentChange}
                        value={comment}
                      ></textarea>
                      <button
                        className={styles.btnTambahKomentar}
                        onClick={handleSubmit}
                      >
                        <span style={{ marginTop: "30px" }}>Tambah</span>
                      </button>
                    </div>
                  </div>
                  {data.balasan?.map((item) => (
                    <a
                      className="text-decoration-none text-dark ml-4 border-left border-light"
                      href={"/forum/komen/" + item.komentar.id}
                    >
                      <div class="card border-0">
                        <div class="card-body">
                          <div className=" d-flex justify-content-start">
                            <img
                              src="https://media.licdn.com/dms/image/D5635AQFnm3Yjq49XMw/profile-framedphoto-shrink_200_200/0/1668648681946?e=1687338000&v=beta&t=p5zZpH4UJIQUIR5xBXgIIEyfl3lqlmrHdf_WHqFxG4U"
                              class="rounded-circle d-block mt-1"
                              alt="profil"
                              height="25"
                              width="25"
                            />
                            <div className="ml-2 d-flex justify-content-start">
                              <h6 class="card-title mt-2">Ariawan</h6>
                              <p
                                class="card-subtitle mb-2 pl-2 mt-2 text-muted"
                                style={{ fontSize: "13px" }}
                              >
                                {item.komentar.createdAt}
                              </p>
                            </div>
                          </div>
                          <p class="mt-1">{item.komentar.konten}</p>
                          <did className="d-flex justify-content-start">
                            <button className={styles.btnKomen}>
                              <i className="mr-1">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  width="16"
                                  height="16"
                                  fill="currentColor"
                                  class="bi bi-plus-lg"
                                  viewBox="0 0 16 16"
                                >
                                  <path
                                    fill-rule="evenodd"
                                    d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"
                                  />
                                </svg>
                              </i>
                              <span style={{ marginTop: "30px" }}>
                                Komentar
                              </span>
                            </button>
                            <div
                              className="ml-3 pt-1"
                              style={{ hover: "ransform: scale(1.5)" }}
                            >
                              <img
                                src="/img/komen.png"
                                width="22"
                                height="22"
                                alt="Komen"
                              />
                              <span class="badge">{item.balasan}</span>
                            </div>
                          </did>
                        </div>
                        {item.KomenBerikutnya?.map((item) => (
                          <a
                            className="text-decoration-none text-dark ml-5 border-left border-dark"
                            href={"/forum/komen/" + item.komentar.id}
                          >
                            <div class="card border-0">
                              <div class="card-body">
                                <div className=" d-flex justify-content-start">
                                  <img
                                    src="https://media.licdn.com/dms/image/D5635AQFnm3Yjq49XMw/profile-framedphoto-shrink_200_200/0/1668648681946?e=1687338000&v=beta&t=p5zZpH4UJIQUIR5xBXgIIEyfl3lqlmrHdf_WHqFxG4U"
                                    class="rounded-circle d-block mt-1"
                                    alt="profil"
                                    height="25"
                                    width="25"
                                  />
                                  <div className="ml-2 d-flex justify-content-start">
                                    <h6 class="card-title mt-2">Ariawan</h6>
                                    <p
                                      class="card-subtitle mb-2 pl-2 mt-2 text-muted"
                                      style={{ fontSize: "13px" }}
                                    >
                                      {item.komentar.createdAt}
                                    </p>
                                  </div>
                                </div>
                                <p class="mt-1">{item.komentar.konten}</p>
                                <did className="d-flex justify-content-start">
                                  <button className={styles.btnKomen}>
                                    <i className="mr-1">
                                      <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="16"
                                        height="16"
                                        fill="currentColor"
                                        class="bi bi-plus-lg"
                                        viewBox="0 0 16 16"
                                      >
                                        <path
                                          fill-rule="evenodd"
                                          d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"
                                        />
                                      </svg>
                                    </i>
                                    <span style={{ marginTop: "30px" }}>
                                      Komentar
                                    </span>
                                  </button>
                                  <div
                                    className="ml-3 pt-1"
                                    style={{ hover: "ransform: scale(1.5)" }}
                                  >
                                    <img
                                      src="/img/komen.png"
                                      width="22"
                                      height="22"
                                      alt="Komen"
                                    />
                                    <span class="badge">{item.balasan}</span>
                                  </div>
                                </did>
                              </div>
                              {item.KomenBerikutnya?.map((item) => (
                                <a
                                  className="text-decoration-none text-dark ml-5 border-left border-dark"
                                  href={"/forum/komen/" + item.komentar.id}
                                >
                                  <div class="card border-0">
                                    <div class="card-body">
                                      <div className=" d-flex justify-content-start">
                                        <img
                                          src="https://media.licdn.com/dms/image/D5635AQFnm3Yjq49XMw/profile-framedphoto-shrink_200_200/0/1668648681946?e=1687338000&v=beta&t=p5zZpH4UJIQUIR5xBXgIIEyfl3lqlmrHdf_WHqFxG4U"
                                          class="rounded-circle d-block mt-1"
                                          alt="profil"
                                          height="25"
                                          width="25"
                                        />
                                        <div className="ml-2 d-flex justify-content-start">
                                          <h6 class="card-title mt-2">
                                            Ariawan
                                          </h6>
                                          <p
                                            class="card-subtitle mb-2 pl-2 mt-2 text-muted"
                                            style={{ fontSize: "13px" }}
                                          >
                                            {item.komentar.createdAt}
                                          </p>
                                        </div>
                                      </div>
                                      <p class="mt-1">{item.komentar.konten}</p>
                                      <did className="d-flex justify-content-start">
                                        <button className={styles.btnKomen}>
                                          <i className="mr-1">
                                            <svg
                                              xmlns="http://www.w3.org/2000/svg"
                                              width="16"
                                              height="16"
                                              fill="currentColor"
                                              class="bi bi-plus-lg"
                                              viewBox="0 0 16 16"
                                            >
                                              <path
                                                fill-rule="evenodd"
                                                d="M8 2a.5.5 0 0 1 .5.5v5h5a.5.5 0 0 1 0 1h-5v5a.5.5 0 0 1-1 0v-5h-5a.5.5 0 0 1 0-1h5v-5A.5.5 0 0 1 8 2Z"
                                              />
                                            </svg>
                                          </i>
                                          <span style={{ marginTop: "30px" }}>
                                            Komentar
                                          </span>
                                        </button>
                                        <div
                                          className="ml-3 pt-1"
                                          style={{
                                            hover: "ransform: scale(1.5)",
                                          }}
                                        >
                                          <img
                                            src="/img/komen.png"
                                            width="22"
                                            height="22"
                                            alt="Komen"
                                          />
                                          <span class="badge">
                                            {item.balasan}
                                          </span>
                                        </div>
                                      </did>
                                    </div>
                                  </div>
                                </a>
                              ))}
                            </div>
                          </a>
                        ))}
                      </div>
                    </a>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>
  );
}

export default Forum;
