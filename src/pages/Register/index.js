import React, { useState } from "react";
import axios from "axios";
import "./register.css";
import { useNavigate } from "react-router-dom";

function Register() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const handleRegister = async (e) => {
    e.preventDefault();

    if (!email || !firstName || !lastName || !password) {
      setError("Please fill in all fields.");
      return;
    }

    if (!/\S+@\S+\.\S+/.test(email)) {
      setError("Please enter a valid email address.");
      return;
    }

    try {
      await axios.post("http://localhost:8000/auth/register", {
        firstName,
        lastName,
        email,
        password,
      });
      navigate("/dashboard");
    } catch (error) {
      if (error.response && error.response.status === 422) {
        setError("Email is already taken. Please choose a different email.");
      } else {
        setError("Registration failed. Please try again.");
      }
    }
  };

  return (
    <body>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4" id="login-slider">
            <div className="container">
              <h1>Ngevent</h1>
              <h2>
                Discover the world's <br />
                With Ngevent
              </h2>
              <img
                className="img-fluid"
                src="https://cdn.dribbble.com/users/3289568/screenshots/6379977/bad_kids.gif"
                alt="Ngevent"
              />
            </div>
            <p>
              Development by Kelompok 4 <br />
              &copy; 2023
            </p>
          </div>
          <div className="col-md-8" id="login-form">
            <div className="container">
              <div className="auth-link">
                <p>
                  Already a member?
                  <a href="/login" className="ml-2">
                    Sign in
                  </a>
                </p>
              </div>
              <form onSubmit={handleRegister}>
                <h3>Sign Up to Ngevent</h3>

                {/* ... */}

                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">Email address</label>
                  <input
                    type="email"
                    className="form-control"
                    id="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputEmail1">First Name</label>
                  <input
                    type="text"
                    className="form-control"
                    id="firstName"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Last Name</label>
                  <input
                    type="text"
                    className="form-control"
                    id="lastName"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="exampleInputPassword1">Password</label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>

                {error && (
                  <p className="error-message" style={{ color: "red" }}>
                    {error}
                  </p>
                )}

                <button type="submit" id="login-btn">
                  Sign Up
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </body>
  );
}

export default Register;
