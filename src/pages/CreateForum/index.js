import React, { useEffect, useState } from "react";
import styles from "./forum.module.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function CreateForum() {
  // const [data, setData] = useState([]);
  const navigate = useNavigate();
  // useEffect(() => {
  //   axios
  //     .get("http://localhost:8000/forum")
  //     .then((response) => {
  //       setData(response.data);
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }, []);

  const [selectedOption, setSelectedOption] = useState("");
  const [inputValue, setInputValue] = useState("");
  const [textareaValue, setTextareaValue] = useState("");

  const handleSelectChange = (event) => {
    setSelectedOption(event.target.value);
  };

  const handleInputChange = (event) => {
    setInputValue(event.target.value);
  };

  const handleTextareaChange = (event) => {
    setTextareaValue(event.target.value);
  };

  const handlePost = async () => {
    const jwtToken = localStorage.getItem("jwtToken");
    const formData = {
      kategoriId: selectedOption,
      judul: inputValue,
      konten: textareaValue,
    };
    console.log(formData);
    try {
      const response = await axios.post(
        "http://localhost:8000/forum",
        formData,
        {
          headers: {
            Authorization: `Bearer ${jwtToken}`,
          },
        }
      );
      if (response.status === 201) {
        return navigate("/forum");
      } else if (response.status === 403) {
        return navigate("/forum/login");
      }
      return navigate("/forum");
    } catch (error) {
      // Handle error
      alert(error);
    }
  };

  return (
    <body className={styles.body}>
      <main>
        <header className={styles.header}>
          <nav className="navbar navbar-expand-lg navbar-light pt-5">
            <div class="container">
              <a class="navbar-brand" href="#">
                Logo
              </a>
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span class="navbar-toggler-icon"></span>
              </button>
              <div
                class="collapse navbar-collapse justify-content-end"
                id="navbarNav"
              >
                <ul class="navbar-nav my-auto">
                  <li class="nav-item active px-3">
                    <a class="nav-link" href="#">
                      Beranda <span class="sr-only"></span>
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Kategori
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Event
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Tentang
                    </a>
                  </li>
                </ul>
                <div class="navbar-nav px-5">
                  <button className={styles.btnLight} onClick="">
                    Masuk
                  </button>
                </div>
              </div>
            </div>
          </nav>
          <div className="container pb-4">
            <h1 className="text-center font-weight-bold pt-3">
              Ngevent Support Community
            </h1>
            <h4 className="text-center">
              Bertanya. Temukan Jawaban. Memecahkan masalah bersama Ngevent.
            </h4>
          </div>
        </header>
        {/* Main Content */}
        <div className="container">
          <div style={{ marginTop: "300px" }}>
            <form className="p-0" onSubmit={handlePost}>
              <div class="form-group">
                <label for="exampleFormControlSelect1">Kategori</label>
                <select
                  class="form-control"
                  id="exampleFormControlSelect1"
                  value={selectedOption}
                  onChange={handleSelectChange}
                >
                  <option value="">Select an option</option>
                  <option value="1">Pembayaran</option>
                  <option value="2">Refund</option>
                  <option value="3">Ide</option>
                  <option value="4">Pelatihan</option>
                </select>
                <div class="form-group mt-3">
                  <label for="exampleFormControlTextarea1">Judul</label>
                  <input
                    type="text"
                    class="form-control"
                    id="exampleInputEmail1"
                    placeholder="Something"
                    value={inputValue}
                    onChange={handleInputChange}
                  ></input>
                </div>
                <div class="form-group mt-3">
                  <label for="exampleFormControlTextarea1">Permasalahan</label>
                  <textarea
                    class="form-control"
                    id="exampleFormControlTextarea1"
                    rows="3"
                    value={textareaValue}
                    onChange={handleTextareaChange}
                  ></textarea>
                </div>
              </div>

              <div className="text-right">
                <button type="submit" className={styles.btnPesan}>
                  Buat Postingan
                </button>
              </div>
            </form>
          </div>
        </div>
      </main>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>
  );
}

export default CreateForum;
