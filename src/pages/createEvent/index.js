import React, { useState } from "react";
import axios from "axios";
import "./createEvent.css";

function CreateEvent() {
  const [selectedImage, setSelectedImage] = useState(null);
  const [formData, setFormData] = useState({
    picture: null,
    name: "",
    decs: "",
    tipe: "",
    isbBerbayar: "",
    harga: "",
    pendaftaran: "",
    tglEvent: "",
  });

  const handleImageUpload = (event) => {
    const file = event.target.files[0];
    setSelectedImage(URL.createObjectURL(file));
    setFormData({
      ...formData,
      picture: file,
    });
  };

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const isFormValid = Object.values(formData).every((value) => value !== "");

    if (!isFormValid) {
      console.log("Please fill in all the fields");
      return;
    }

    try {
      const jwtToken = localStorage.getItem("jwtToken");
      const headers = {
        Authorization: `Bearer ${jwtToken}`,
      };

      const response = await axios.post(
        "http://localhost:8000/event/create",
        formData,
        { headers }
      );

      console.log(response.data); // Handle the response as needed
      setSelectedImage(null);
      setFormData({
        picture: null,
        eventName: "",
        description: "",
        organizer: "",
        ticketType: "",
        price: "",
        registrationDate: "",
        eventDate: "",
      });
    } catch (error) {
      console.error(error);
      // Handle error cases
    }
  };

  return (
    <div className="body">
      <div className="row vh-100">
        <div className="col-2 nopadding vh-100">
          <div className="sidebar">
            <div className="pl-4">
              <div className="container">
                <h2 className="text-center text-dark">Ngevent</h2>
              </div>
              <ul className="ul-nav">
                <h6 className="text-muted"> MAIN MENU</h6>
                <li className="li-nav">
                  <span className="pr-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      className="bi bi-ticket-perforated-fill"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 4.5A1.5 1.5 0 0 1 1.5 3h13A1.5 1.5 0 0 1 16 4.5V6a.5.5 0 0 1-.5.5 1.5 1.5 0 0 0 0 3 .5.5 0 0 1 .5.5v1.5a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 11.5V10a.5.5 0 0 1 .5-.5 1.5 1.5 0 1 0 0-3A.5.5 0 0 1 0 6V4.5Zm4-1v1h1v-1H4Zm1 3v-1H4v1h1Zm7 0v-1h-1v1h1Zm-1-2h1v-1h-1v1Zm-6 3H4v1h1v-1Zm7 1v-1h-1v1h1Zm-7 1H4v1h1v-1Zm7 1v-1h-1v1h1Zm-8 1v1h1v-1H4Zm7 1h1v-1h-1v1Z" />
                    </svg>
                  </span>
                  <a href="#">My Ticket</a>
                </li>
                <li className="li-nav mt-4">
                  <span className="pr-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      className="bi bi-credit-card-fill"
                      viewBox="0 0 16 16"
                    >
                      <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1H0V4zm0 3v5a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7H0zm3 2h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1z" />
                    </svg>
                  </span>
                  <a href="/data">Account</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="col-10 pb-5 px-5 main">
          <div>
            <div className="row">
              <div className="col-4 my-4">
                <h2 className="font-weight-bold text-dark ">Event</h2>
              </div>
              <div className="col-8">
                <div className="d-flex justify-content-end">
                  <div className="circle-container my-4">
                    <img
                      src="https://images.unsplash.com/photo-1606413712024-0df0371d35a9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8b3Jhbmd8ZW58MHx8MHx8fDA%3D&w=1000&q=80"
                      className="img-fluid img-profil"
                      alt="..."
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="create-event  px-5 py-5">
            <h4 className="poster"> Poster Event</h4>
            <div>
              {selectedImage && (
                <img
                  src={selectedImage}
                  className="img-fluid mb-4"
                  width={400}
                  alt="Uploaded Image"
                />
              )}
              <br></br>
              <input
                type="file"
                onChange={handleImageUpload}
                className=""
                accept="image/*"
              />
            </div>
            <h4 className="poster mt-4"> Poster Event</h4>
            <select
              name="jenis"
              className="w-100 px-2 py-2"
              id="jenis-event"
              value={formData.jenis}
              onChange={handleChange}
            >
              <option value="1">Adaseminar</option>
              <option value="2">Webinar</option>
              <option value="3">Lomba</option>
              <option value="4">Pelatihan</option>
            </select>
            <h4 className="poster mt-4"> Nama Event</h4>
            <input
              type="text"
              className="input-event"
              name="name"
              value={formData.name}
              onChange={handleChange}
            />
            <h4 className="poster mt-4"> Deskripsi</h4>
            <textarea
              id="w3review"
              name="decs"
              rows="10"
              cols="50"
              className="w-100"
              value={formData.decs}
              onChange={handleChange}
            ></textarea>
            <h4 className="poster mt-4"> Penyelenggara</h4>
            <input
              type="text"
              className="input-event"
              name="penyelenggara"
              value={formData.penyelenggara}
              onChange={handleChange}
            />
            <h4 className="poster mt-4"> Link Buku Panduan</h4>
            <input
              type="text"
              className="input-event"
              name="link"
              value={formData.link}
              onChange={handleChange}
            />
            <h4 className="poster mt-4"> Tipe Event</h4>
            <div>
              <div className="form-check form-check-inline mt-2">
                <input
                  className="form-check-input"
                  type="radio"
                  name="tipe"
                  id="online"
                  value="online"
                  checked={formData.tipe === "online"}
                  onChange={handleChange}
                />
                <label className="form-check-label" htmlFor="online">
                  Online
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="tipe"
                  id="offline"
                  value="offline"
                  checked={formData.tipe === "offline"}
                  onChange={handleChange}
                />
                <label className="form-check-label" htmlFor="offline">
                  Offline
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="tipe"
                  id="hybrid"
                  value="hybrid"
                  checked={formData.tipe === "hybrid"}
                  onChange={handleChange}
                />
                <label className="form-check-label" htmlFor="hybrid">
                  Hybrid
                </label>
              </div>
            </div>
            <h4 className="poster mt-4"> Tipe Tiket</h4>
            <div>
              <div className="form-check form-check-inline mt-2">
                <input
                  className="form-check-input"
                  type="radio"
                  name="isbBerbayar"
                  id="free"
                  value="free"
                  checked={formData.isbBerbayar === "false"}
                  onChange={handleChange}
                />
                <label className="form-check-label" htmlFor="free">
                  Gratis
                </label>
              </div>
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input"
                  type="radio"
                  name="isbBerbayar"
                  id="paid"
                  value="paid"
                  checked={formData.isbBerbayar === "true"}
                  onChange={handleChange}
                />
                <label className="form-check-label" htmlFor="paid">
                  Berbayar
                </label>
              </div>
            </div>
            <h4 className="poster mt-4"> Harga</h4>
            <input
              type="number"
              className="input-event"
              name="harga"
              value={formData.harga}
              onChange={handleChange}
            />
            <h4 className="poster mt-4"> Pendaftaran</h4>
            <input
              type="date"
              className="input-event"
              name="pendaftaran"
              value={formData.pendaftaran}
              onChange={handleChange}
            />
            <h4 className="poster mt-4"> Hari-H</h4>
            <input
              type="date"
              className="input-event"
              name="tglEvent"
              value={formData.tglEvent}
              onChange={handleChange}
            />
            <div className="text-center">
              <button
                type="submit"
                onClick={handleSubmit}
                className="btn btn-primary mt-5"
              >
                Tambahkan Event
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateEvent;
