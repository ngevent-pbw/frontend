import React, { useEffect, useState } from "react";
import styles from "./forum.module.css";
import axios from "axios";
import { useNavigate } from "react-router-dom";

function Forum() {
  const [data, setData] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    axios
      .get("http://localhost:8000/forum")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const handleCreatePost = async () => {
    const jwtToken = localStorage.getItem("jwtToken");
    const headers = {
      Authorization: `Bearer ${jwtToken}`,
    };
    await axios
      .get("http://localhost:8000/auth/", { headers })
      .then((response) => {
        if (response.status === 200) {
          return navigate("/forum/create");
        } else if (response.status === 403) {
          return navigate("/forum/login");
        }
      })
      .catch((error) => {
        return navigate("/login");
      });
  };
  const handleClickForum = (id) => {
    return navigate("/forum/" + id);
  };

  return (
    <body className={styles.body}>
      <main>
        <header className={styles.header}>
          <nav className="navbar navbar-expand-lg navbar-light pt-5">
            <div class="container">
              <a class="navbar-brand" href="#">
                Logo
              </a>
              <button
                class="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span class="navbar-toggler-icon"></span>
              </button>
              <div
                class="collapse navbar-collapse justify-content-end"
                id="navbarNav"
              >
                <ul class="navbar-nav my-auto">
                  <li class="nav-item active px-3">
                    <a class="nav-link" href="#">
                      Beranda <span class="sr-only"></span>
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Kategori
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Event
                    </a>
                  </li>
                  <li class="nav-item px-3">
                    <a class="nav-link" href="#">
                      Tentang
                    </a>
                  </li>
                </ul>
                <div class="navbar-nav px-5">
                  <button className={styles.btnLight} onClick="">
                    Masuk
                  </button>
                </div>
              </div>
            </div>
          </nav>
          <div className="container pb-4">
            <h1 className="text-center font-weight-bold pt-3">
              Ngevent Support Community
            </h1>
            <h4 className="text-center">
              Bertanya. Temukan Jawaban. Memecahkan masalah bersama Ngevent.
            </h4>
          </div>
        </header>
        {/* Main Content */}
        <div className="container">
          <div
            className="text-right"
            style={{ marginTop: "250px", marginRight: "49px" }}
          >
            <button onClick={handleCreatePost} className={styles.btnPesan}>
              Buat Postingan
            </button>
          </div>

          <div className="mt-4">
            <div className="row">
              <div className="col-2">
                <div className="position-fixed">
                  <input
                    className={styles.Search}
                    type="text"
                    placeholder="Cari Topik"
                  />
                  <ul class="list-group mt-4">
                    <li class="my-4 py-2  px-3 d-flex justify-content-between align-items-center selected">
                      All
                      <span class="badge badge-light badge-pill">14</span>
                    </li>
                    <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # Pembayaran
                      <span class="badge badge-light badge-pill">2</span>
                    </li>
                    <li class=" my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # refund
                      <span class="badge badge-light badge-pill">1</span>
                    </li>
                    <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # ide
                      <span class="badge badge-light badge-pill">1</span>
                    </li>
                    <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                      # pelatihan
                      <span class="badge badge-light badge-pill">1</span>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="col-9" style={{ marginLeft: "50px" }}>
                {data.map((item) => (
                  <a
                    className="text-decoration-none text-dark"
                    href={"/forum/komen/" + item.id}
                  >
                    <div class="card mb-4">
                      <div class="card-body">
                        <h4 class="pt-1 font-weight-bold">{item.judul}</h4>
                        <div className="mt-3 d-flex justify-content-start">
                          <img
                            src="https://media.licdn.com/dms/image/D5635AQFnm3Yjq49XMw/profile-framedphoto-shrink_200_200/0/1668648681946?e=1687338000&v=beta&t=p5zZpH4UJIQUIR5xBXgIIEyfl3lqlmrHdf_WHqFxG4U"
                            class="rounded-circle d-block"
                            alt="profil"
                            height="50"
                            width="50"
                          />
                          <div className="ml-2">
                            <h6 class="card-title">Ariawan</h6>
                            <p
                              class="card-subtitle mb-2 text-muted"
                              style={{ fontSize: "13px" }}
                            >
                              {item.createdAt}
                            </p>
                          </div>
                        </div>
                        <p class="mt-2">{item.konten}</p>
                        <div style={{ hover: "ransform: scale(1.5)" }}>
                          <img
                            src="/img/komen.png"
                            width="22"
                            height="22"
                            alt="Komen"
                          />
                          <span class="badge">{item.totalKomentar}</span>
                        </div>
                      </div>
                    </div>
                  </a>
                ))}
              </div>
            </div>
          </div>
        </div>
      </main>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </body>
  );
}

export default Forum;
