import React, { useState, useEffect } from "react";
import axios from "axios";

import { useNavigate } from "react-router-dom";
import "./Account.css";

function Account() {
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const [selectedImage, setSelectedImage] = useState(
    "https://images.unsplash.com/photo-1606413712024-0df0371d35a9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8b3Jhbmd8ZW58MHx8MHx8fDA%3D&w=1000&q=80"
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        const token = localStorage.getItem("jwtToken");
        const config = {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        };

        const response = await axios.get("http://localhost:8000/user/", config);
        const { email, name, image } = response.data.data;
        setEmail(email);
        setName(name);
        setSelectedImage(image);
      } catch (error) {
        setError("Failed to fetch account data.");
      }
    };

    fetchData();
  }, []);

  const handleImageChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      setSelectedImage(URL.createObjectURL(file));
    }
  };

  return (
    <div className="body">
      <div class="row vh-100">
        <div class="col-2 nopadding vh-100">
          <div class="sidebar">
            <div class="pl-4">
              <div class="container">
                <h2 class="text-center text-dark">Ngevent</h2>
              </div>
              <ul class="ul-nav">
                <h6 class="text-muted"> MAIN MENU</h6>
                <li class="li-nav">
                  <span class="pr-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      class="bi bi-ticket-perforated-fill"
                      viewbox="0 0 16 16"
                    >
                      <path d="M0 4.5A1.5 1.5 0 0 1 1.5 3h13A1.5 1.5 0 0 1 16 4.5V6a.5.5 0 0 1-.5.5 1.5 1.5 0 0 0 0 3 .5.5 0 0 1 .5.5v1.5a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 11.5V10a.5.5 0 0 1 .5-.5 1.5 1.5 0 1 0 0-3A.5.5 0 0 1 0 6V4.5Zm4-1v1h1v-1H4Zm1 3v-1H4v1h1Zm7 0v-1h-1v1h1Zm-1-2h1v-1h-1v1Zm-6 3H4v1h1v-1Zm7 1v-1h-1v1h1Zm-7 1H4v1h1v-1Zm7 1v-1h-1v1h1Zm-8 1v1h1v-1H4Zm7 1h1v-1h-1v1Z" />
                    </svg>
                  </span>
                  <a href="/dashboard">My Ticket</a>
                </li>
                <li class="li-nav mt-4">
                  <span class="pr-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      class="bi bi-credit-card-fill"
                      viewbox="0 0 16 16"
                    >
                      <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1H0V4zm0 3v5a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7H0zm3 2h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1z" />
                    </svg>
                  </span>
                  <a href="/account">Account</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-10 nopadding vh-100 px-5">
          <div>
            <div class="row">
              <div class="col-4 my-4">
                <h2 class="font-weight-bold text-dark ">Account</h2>
              </div>
              <div class="col-8">
                <div class="d-flex justify-content-end">
                  <div class="circle-container my-4">
                    <img
                      src="https://images.unsplash.com/photo-1606413712024-0df0371d35a9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8b3Jhbmd8ZW58MHx8MHx8fDA%3D&w=1000&q=80"
                      class="img-fluid img-profil"
                      alt="..."
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="px-5">
            <div className="form-group">
              <div class="d-flex justify-content-center">
                <div class=" img-profiul my-4">
                  <input
                    type="file"
                    accept="image/*"
                    onChange={handleImageChange}
                    style={{ display: "hidden" }}
                  />
                  {selectedImage && (
                    <img src={selectedImage} class=" img-fluid" alt="..." />
                  )}
                </div>
              </div>
              <label htmlFor="email">Email address</label>
              <input
                type="email"
                className="form-control"
                id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>

            {/* Password field */}
            <div className="form-group">
              <label htmlFor="password">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </div>

            {/* Error message */}
            {error && (
              <div className="error" style={{ color: "red" }}>
                {error}
              </div>
            )}

            {/* Submit button */}
            <button type="submit" id="login-btn">
              Simpan
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Account;
