import React from "react";
import "./addEvent.css";
function addEvent() {
  return (
    <div className="body">
      <div class="row vh-100">
        <div class="col-2 nopadding vh-100">
          <div class="sidebar">
            <div class="pl-4">
              <div class="container">
                <h2 class="text-center text-dark">Ngevent</h2>
              </div>
              <ul class="ul-nav">
                <h6 class="text-muted"> MAIN MENU</h6>
                <li class="li-nav">
                  <span class="pr-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      class="bi bi-ticket-perforated-fill"
                      viewbox="0 0 16 16"
                    >
                      <path d="M0 4.5A1.5 1.5 0 0 1 1.5 3h13A1.5 1.5 0 0 1 16 4.5V6a.5.5 0 0 1-.5.5 1.5 1.5 0 0 0 0 3 .5.5 0 0 1 .5.5v1.5a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 11.5V10a.5.5 0 0 1 .5-.5 1.5 1.5 0 1 0 0-3A.5.5 0 0 1 0 6V4.5Zm4-1v1h1v-1H4Zm1 3v-1H4v1h1Zm7 0v-1h-1v1h1Zm-1-2h1v-1h-1v1Zm-6 3H4v1h1v-1Zm7 1v-1h-1v1h1Zm-7 1H4v1h1v-1Zm7 1v-1h-1v1h1Zm-8 1v1h1v-1H4Zm7 1h1v-1h-1v1Z" />
                    </svg>
                  </span>
                  <a href="#">My Ticket</a>
                </li>
                <li class="li-nav mt-4">
                  <span class="pr-3">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="20"
                      height="20"
                      fill="currentColor"
                      class="bi bi-credit-card-fill"
                      viewbox="0 0 16 16"
                    >
                      <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1H0V4zm0 3v5a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7H0zm3 2h1a1 1 0 0 1 1 1v1a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1v-1a1 1 0 0 1 1-1z" />
                    </svg>
                  </span>
                  <a href="/data">Account</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-10 nopadding vh-100 px-5">
          <div>
            <div class="row">
              <div class="col-4 my-4">
                <h2 class="font-weight-bold text-dark ">Event</h2>
              </div>
              <div class="col-8">
                <div class="d-flex justify-content-end">
                  <div class="circle-container my-4">
                    <img
                      src="https://images.unsplash.com/photo-1606413712024-0df0371d35a9?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8b3Jhbmd8ZW58MHx8MHx8fDA%3D&w=1000&q=80"
                      class="img-fluid img-profil"
                      alt="..."
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tabel px-3 py-3">
            <div class="row">
              <div class="col-5">
                <h4> Data Tabel</h4>
              </div>
              <div class="col-7">
                <div class="text-right">
                  <button type="button" class="btn btn-primary ">
                    Tambah Event
                  </button>
                </div>
              </div>
            </div>
            <table class="table table-bordered mt-3">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">First</th>
                  <th scope="col">Last</th>
                  <th scope="col">Handle</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td colspan="2">Larry the Bird</td>
                  <td>@twitter</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default addEvent;
