import React, { useState } from "react";
import axios from "axios";
import "./login.css";
import { useNavigate } from "react-router-dom";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    setError("");

    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setError("Invalid email address");
      return;
    }

    if (!email || !password) {
      setError("Email and password are required");
      return;
    }

    try {
      const response = await axios.post("http://localhost:8000/auth/login", {
        email: email,
        password: password,
      });

      const token = response.data.token;
      localStorage.setItem("jwtToken", token);

      console.log("Login berhasil!", response.data);
      return navigate("/forum");
    } catch (error) {
      setError("Username or password is incorrect");
    }
  };

  return (
    <body>
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4" id="login-slider">
            <div className="container">
              <h1>Ngevent</h1>
              <h2>
                Discover the world's <br />
                With Ngevent
              </h2>
              <img
                className="img-fluid"
                src="https://cdn.dribbble.com/users/3289568/screenshots/6379977/bad_kids.gif"
                alt="Ngevent"
              />
            </div>
            <p>
              Development by Kelompok 4 <br />
              &copy; 2023
            </p>
          </div>
          <div className="col-md-8" id="login-form">
            <div className="container">
              <div className="auth-link">
                <p>
                  Not a member?
                  <a href="/register" className="ml-2">
                    Sign up now
                  </a>
                </p>
              </div>
              <form onSubmit={handleLogin}>
                <h3>Sign in to Ngevent</h3>

                {/* Google button */}
                <div className="col-sm-8 col-md-8 col-lg-8">
                  <button className="google-btn">
                    <img
                      src="https://www.gstatic.com/firebasejs/ui/2.0.0/images/auth/google.svg"
                      alt="Google logo"
                    />
                    <span>Login with Google</span>
                  </button>
                </div>

                <div className="separator">Or</div>

                {/* Email field */}
                <div className="form-group">
                  <label htmlFor="email">Email address</label>
                  <input
                    type="email"
                    className="form-control"
                    id="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </div>

                {/* Password field */}
                <div className="form-group">
                  <label htmlFor="password">Password</label>
                  <input
                    type="password"
                    className="form-control"
                    id="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </div>

                {/* Error message */}
                {error && (
                  <div className="error" style={{ color: "red" }}>
                    {error}
                  </div>
                )}

                {/* Submit button */}
                <button type="submit" id="login-btn">
                  Sign in
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <script></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </body>
  );
}

export default Login;
