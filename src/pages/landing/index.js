import React, { useEffect } from "react";
import styles from "./landing.module.css";
import { useNavigate } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination, Navigation } from "swiper";

function Landing() {
  useEffect(() => {}, []);
  const navigate = useNavigate();
  
  const onForum = () => {
    return navigate("/forum");
  };

  return (
    <>
      <body className={styles.body}>
        <main>
          <header className={styles.header}>
            <nav className="navbar navbar-expand-lg navbar-light py-5">
              <div class="container">
                <a class="navbar-brand" href="/">
                  Logo
                </a>
                <button
                  class="navbar-toggler"
                  type="button"
                  data-toggle="collapse"
                  data-target="#navbarNav"
                  aria-controls="navbarNav"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div
                  class="collapse navbar-collapse justify-content-end"
                  id="navbarNav"
                >
                  <ul class="navbar-nav my-auto">
                    <li class="nav-item active px-3">
                      <a class="nav-link" href="#">
                        Beranda <span class="sr-only"></span>
                      </a>
                    </li>
                    <li class="nav-item px-3">
                      <a class="nav-link" href="#">
                        Kategori
                      </a>
                    </li>
                    <li class="nav-item px-3">
                      <a class="nav-link" href="#">
                        Event
                      </a>
                    </li>
                    <li class="nav-item px-3">
                      <a class="nav-link" href="#">
                        Tentang
                      </a>
                    </li>
                  </ul>
                  <div class="navbar-nav px-5">
                    <a href="/login">
                      <button className={styles.btnLight}>Masuk</button>
                    </a>
                  </div>
                </div>
              </div>
            </nav>
            <div className="container mt-4 baner pt-2">
              <div className="row">
                <div className="col-6 mt-5">
                  <h1 className={styles.titleBanner}>
                    Temukan Eventmu, Tingkatkan Skillsmu
                  </h1>
                  <p className="py-2 pr-3">
                    Siap memulai, menemukan, atau meningkatkan keterampilan
                    Kamu? NgeventYuks menawarkan berbagai event di seluruh
                    Indonesia dalam bidang sains, teknologi, sosial, dan masih
                    banyak lagi
                  </p>
                  <button
                    type="botton"
                    onClick={onForum}
                    className={styles.btnLight}
                  >
                    Ayo Temukan!
                  </button>
                </div>
                <div className="col-6">
                  <img className="img-fluid" src="img/baner.png" />
                </div>
              </div>
            </div>
          </header>
          {/* Main Content */}
          <div className="container mt-4">
            <div className="row">
              <div className="col-3">
                <input
                  className={styles.Search}
                  type="text"
                  placeholder="Cari Events"
                />
                <ul class="list-group mt-4">
                  <li class="my-4 py-2  px-3 d-flex justify-content-between align-items-center selected">
                    All
                    <span class="badge badge-light badge-pill">14</span>
                  </li>
                  <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                    Seminar
                    <span class="badge badge-light badge-pill">2</span>
                  </li>
                  <li class=" my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                    Webinar
                    <span class="badge badge-light badge-pill">1</span>
                  </li>
                  <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                    Lomba
                    <span class="badge badge-light badge-pill">1</span>
                  </li>
                  <li class="my-4  py-2  px-3 d-flex justify-content-between align-items-center">
                    Pelatihan
                    <span class="badge badge-light badge-pill">1</span>
                  </li>
                </ul>
              </div>
              <div className="col-9">
                <h3 className="font-weight-bold mb-3">Rekomendasi Seminar</h3>

                <Swiper
                  spaceBetween={15}
                  slidesPerView={3}
                  loop={true}
                  centeredSlides
                  autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                    reverseDirection: true,
                  }}
                  modules={[Autoplay, Pagination, Navigation]}
                  className="mySwiper"
                >
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                </Swiper>

                <h6 className="text-right font-weight-bold pt-3">
                  Lihat Semua
                </h6>
                <h3 className="font-weight-bold mb-3 mt-5">
                  Rekomendasi Webinar
                </h3>
                <Swiper
                  spaceBetween={15}
                  slidesPerView={3}
                  loop={true}
                  centeredSlides
                  autoplay={{
                    delay: 3000,
                    disableOnInteraction: false,
                  }}
                  modules={[Autoplay, Pagination, Navigation]}
                  className="mySwiper"
                >
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                </Swiper>
                <h6 className="text-right font-weight-bold pt-3">
                  Lihat Semua
                </h6>
                <h3 className="font-weight-bold mb-3 mt-5">
                  Rekomendasi Lomba
                </h3>
                <Swiper
                  spaceBetween={15}
                  slidesPerView={3}
                  loop={true}
                  centeredSlides
                  autoplay={{
                    delay: 3500,
                    disableOnInteraction: false,
                    reverseDirection: true,
                  }}
                  modules={[Autoplay, Pagination, Navigation]}
                  className="mySwiper"
                >
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                </Swiper>
                <h6 className="text-right font-weight-bold pt-3">
                  Lihat Semua
                </h6>
                <h3 className="font-weight-bold mb-3 mt-5">
                  Rekomendasi Pelatihan
                </h3>
                <Swiper
                  spaceBetween={15}
                  slidesPerView={3}
                  loop={true}
                  centeredSlides
                  autoplay={{ delay: 4500, disableOnInteraction: false }}
                  modules={[Autoplay, Pagination, Navigation]}
                  className="mySwiper"
                >
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div class="card">
                      <div class="card-body p-0">
                        <img class="img-fluid" src="img/invasi.png" />
                        <div className="mt-2">
                          <span
                            className="ml-3"
                            id={styles.label}
                            style={{ backgroundColor: "#1D1D1D" }}
                          >
                            Webinar
                          </span>
                          <span
                            className="ml-1"
                            id={styles.label}
                            style={{ backgroundColor: "#83067F" }}
                          >
                            Berbayar
                          </span>
                        </div>
                        <h5 class="font-weight-bold mt-3 pl-3">
                          INVASI UDAYANA
                        </h5>
                        <div className="pl-3 d-flex align-content-between">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="16"
                            fill="currentColor"
                            class="bi bi-calendar pt-1"
                            viewBox="0 0 16 16"
                          >
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
                          </svg>
                          <p class="card-text pl-2">24 Oct - 17 Nov 2022</p>
                        </div>
                        <div class="text-center mb-3 mt-3">
                          <button className={styles.btnPesan}>
                            Daftar Sekarang
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                </Swiper>
                <h6 className="text-right font-weight-bold pt-3">
                  Lihat Semua
                </h6>
              </div>
            </div>
          </div>
          <div className="container my-5">
            <div className={styles.mainForum}>
              <div className="row">
                <div className="col-6 text-center">
                  <h1 className={styles.titleForum}>Ada Pertanyaan?</h1>
                  <a href="/forum">
                    <button className={styles.btnForum}>Ayo Ke Forum</button>
                  </a>
                </div>
                <div className="col-6">
                  <img src="/img/Group.png" className="img-fluid"></img>
                </div>
              </div>
            </div>
          </div>
          {/* <div class="container-fluid baner-bawah mt-5">
          <div class="container">
            <div class="row">
              <div class="col-6">
                <h1>Tentang Kami</h1>
                <p class="pt-2">
                  Ngevent Yuks" adalah sebuah platform media informasi yang
                  berfokus pada penyebaran informasi lomba, webinar, seminar,
                  dan pelatihan. Aplikasi “Ngevent Yuk” diharapkan dapat
                  mengajak para mahasiswa di seluruh Indonesia untuk mengikuti
                  event guna menguasai dan mengasah kemampuan hardskill maupun
                  softskill.
                </p>
              </div>
              <div class="offset-1 col-5">
                <img class="img-fluid" src="img/bawah-1.png" />
              </div>
            </div>
          </div>
        </div> */}
        </main>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
      </body>
    </>
  );
}

export default Landing;
