import { BrowserRouter, Routes, Route } from "react-router-dom";

import Landing from "./pages/landing";
import Forum from "./pages/Forum";
import CreateForum from "./pages/CreateForum";
import Register from "./pages/Register";
import Login from "./pages/login";
import Komen from "./pages/Komen";
import Dashboard from "./pages/Dashboard";
import AddEvent from "./pages/addEvent";
import Account from "./pages/Account";
import CreateEvent from "./pages/createEvent";

import "swiper/css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    // <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route path="/forum" element={<Forum />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/account" element={<Account />} />
        <Route path="/forum/create" element={<CreateForum />} />
        <Route path="/forum/create" element={<CreateForum />} />
        <Route path="/forum/komen/:id" element={<Komen />} />
        <Route path="/addEvent" element={<AddEvent />} />
        <Route path="/createEvent" element={<CreateEvent />} />
      </Routes>
    </BrowserRouter>
    // </Provider>
  );
}

export default App;
